drop table if exists role;

create table role
(
    id   serial primary key,
    name varchar(300) not null,
    UNIQUE (name)
);

drop table if exists users;

create table users
(
    id       uuid primary key default gen_random_uuid(),
    login    varchar(300) not null,
    password varchar(300) not null,
    role_id  integer      not null,
    UNIQUE (login),

    CONSTRAINT fk_role FOREIGN KEY (role_id) REFERENCES role(id)
);

insert into role (name)
VALUES ('admin'),
       ('owner'),
       ('reader');

insert into users (login, password, role_id)
VALUES ('admin', '$2a$10$CbnELvgk3CH.EARIURIEU.JQluKyClaoWId0mQNmtHe3aqIeA4MrW', (
        (select id from role where name = 'admin' limit 1)));
-- pass: `admin_pass`