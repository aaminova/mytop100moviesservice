ThisBuild / scalaVersion := "2.13.10"

lazy val circeVersion               = "0.14.3"
lazy val tapirVersion               = "1.2.8"
lazy val zioVersion                 = "2.0.8"
lazy val scalacVersion              = "2.0.8"
lazy val derevoVersion              = "0.13.0"
lazy val enumeratumVersion          = "1.7.0"
lazy val catsVersion                = "2.8.0"
lazy val doobieVersion              = "1.0.0-RC2"
lazy val quillVersion               = "4.6.0"
lazy val `zio-config`               = "3.0.7"
lazy val testcontainersScalaVersion = "0.40.12"
lazy val flywayVersion              = "9.15.2"
lazy val tofuVersion                = "0.11.1"
lazy val zioOpentelemetryVersion    = "3.0.0-RC3"
lazy val akkaHttpMetricsVersion     = "1.7.1"

lazy val testSetting = Seq(
  libraryDependencies ++= Seq(
    "com.dimafeng"                %% "testcontainers-scala-scalatest"    % testcontainersScalaVersion,
    "com.dimafeng"                %% "testcontainers-scala-postgresql"   % testcontainersScalaVersion,
    "io.github.scottweaver"       %% "zio-2-0-testcontainers-postgresql" % "0.10.0",
    "com.softwaremill.sttp.tapir" %% "tapir-sttp-stub-server"            % tapirVersion % Test,
    "org.scalatest"               %% "scalatest"                         % "3.2.15"     % Test,
    "dev.zio"                     %% "zio-test"                          % zioVersion   % Test,
    "dev.zio"                     %% "zio-test-magnolia"                 % zioVersion   % Test,
    "dev.zio"                     %% "zio-test-sbt"                      % zioVersion   % Test,
    "org.scalatestplus"           %% "mockito-4-6"                       % "3.2.15.0"   % "test",
  )
)

lazy val zioSetting = Seq(
  libraryDependencies ++= Seq(
    "dev.zio" %% "zio"                 % zioVersion,
    "dev.zio" %% "zio-config"          % `zio-config`,
    "dev.zio" %% "zio-config-typesafe" % `zio-config`,
    "dev.zio" %% "zio-config-magnolia" % `zio-config`,
    "dev.zio" %% "zio-interop-cats"    % "23.0.0.0"
  )
)

lazy val httpSetting = Seq(
  libraryDependencies ++= Seq(
    "com.softwaremill.sttp.tapir"   %% "tapir-akka-http-server"       % tapirVersion,
    "com.softwaremill.sttp.tapir"   %% "tapir-json-circe"             % tapirVersion,
    "com.softwaremill.sttp.tapir"   %% "tapir-derevo"                 % tapirVersion,
    "com.softwaremill.sttp.tapir"   %% "tapir-swagger-ui-bundle"      % tapirVersion,
    "com.softwaremill.sttp.tapir"   %% "tapir-sttp-stub-server"       % tapirVersion,
    "com.softwaremill.sttp.tapir"   %% "tapir-prometheus-metrics"     % tapirVersion,
    "com.softwaremill.sttp.client3" %% "circe"                        % "3.8.11",
    "io.circe"                      %% "circe-core"                   % circeVersion,
    "io.circe"                      %% "circe-generic"                % circeVersion,
    "org.typelevel"                 %% "cats-core"                    % catsVersion,
    "tf.tofu"                       %% "derevo-circe"                 % derevoVersion,
    "tf.tofu"                       %% "derevo-cats"                  % derevoVersion,
    "com.typesafe.scala-logging"    %% "scala-logging"                % "3.9.5",
    "org.slf4j"                      % "slf4j-api"                    % "2.0.5",
    "ch.qos.logback"                 % "logback-classic"              % "1.4.5",
    "tf.tofu"                       %% "tofu-core-ce3"                % tofuVersion,
    "tf.tofu"                       %% "tofu-logging-derivation"      % tofuVersion,
    "tf.tofu"                       %% "tofu-logging-layout"          % tofuVersion,
    "tf.tofu"                       %% "tofu-logging-structured"      % tofuVersion,
    "dev.zio"                       %% "zio-opentelemetry"            % zioOpentelemetryVersion,
    "fr.davit"                      %% "akka-http-metrics-prometheus" % akkaHttpMetricsVersion,
    "org.mindrot"                    % "jbcrypt"                      % "0.4",
  )
)

lazy val sqlSetting = Seq(
  libraryDependencies ++= Seq(
    "org.tpolecat" %% "doobie-core"      % doobieVersion,
    "org.tpolecat" %% "doobie-hikari"    % doobieVersion,
    "org.tpolecat" %% "doobie-postgres"  % doobieVersion,
    "org.tpolecat" %% "doobie-h2"        % doobieVersion,
    "org.tpolecat" %% "doobie-scalatest" % doobieVersion % Test,
    "io.getquill"  %% "quill-doobie"     % quillVersion,
    "org.flywaydb"  % "flyway-core"      % flywayVersion % Test,
  )
)

lazy val root = (project in file("."))
  .settings(name := "moviesapp", version := "0.2.0-SNAPSHOT")
  .settings(zioSetting, httpSetting, sqlSetting, testSetting)
  .settings(
    dependencyOverrides += "io.circe" %% "circe-core" % circeVersion,
  )
  .settings(
    scalacOptions += "-Ymacro-annotations"
  )
  .settings(testFrameworks += new TestFramework("zio.test.sbt.ZTestFramework"))
  .settings(
    dockerExposedPorts := Seq(8080),
    dockerBaseImage := "eclipse-temurin:11",
    dockerEntrypoint := Seq("/opt/docker/bin/main")
  )
  .settings(Compile / unmanagedResourceDirectories ++= Seq((ThisBuild / baseDirectory).value / "sql"))
  .settings(Test / fork := true)
  .enablePlugins(DockerPlugin, JavaAppPackaging)
