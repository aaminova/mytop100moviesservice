package movies.db

import java.util.UUID

import com.dimafeng.testcontainers.PostgreSQLContainer
import doobie.implicits._
import doobie.postgres.implicits._
import io.github.scottweaver.zio.testcontainers.postgres.ZPostgreSQLContainer
import movies.configuration.DbConfig
import movies.db.user.UserDaoImpl
import movies.model._
import org.flywaydb.core.Flyway
import org.mindrot.jbcrypt.BCrypt
import zio.Task
import zio.ZIO
import zio.durationInt
import zio.test.Assertion.equalTo
import zio.test._

class UserDaoSpec extends ZIOSpecDefault {

  private val adminRole   = Role(Role.Id(1), Role.Type.Admin.toText)
  private val ownerRole   = Role(Role.Id(2), Role.Type.Owner.toText)
  private val readeerRole = Role(Role.Id(3), Role.Type.Reader.toText)

  val admin: User  = User(User.Id.random(), "god", hashedPassword("admin_pass"), adminRole.id)
  val owner: User  = User(User.Id.random(), "blogger", hashedPassword("owner_pass"), ownerRole.id)
  val reader: User = User(User.Id.random(), "mimo_crocodil", hashedPassword("reader_pass"), readeerRole.id)

  class Context(container: PostgreSQLContainer) {
    import zio.interop.catz.asyncInstance

    Class.forName(container.driverClassName)

    val tm = new TransactorManagerImpl(
      DbConfig(container.driverClassName, container.jdbcUrl, container.username, container.password)
    )
    val transactor = tm.getTransactor
    val userDao    = new UserDaoImpl(tm)

    val flywayConfig = Flyway.configure
      .locations("filesystem:./sql/migrations")
      .dataSource(container.jdbcUrl, container.username, container.password)
    val flyway = flywayConfig.load()
    flyway.migrate()

    def truncate = sql"truncate table users".update.run.transact(transactor)

    def populate(id: UUID, login: String, password: String, roleId: Int): Task[Int] =
      sql"INSERT INTO users (id, login, password, role_id) VALUES ($id, $login, $password, $roleId)".update.run
        .transact(transactor)

    def populate(u: User): Task[Int] = populate(u.id.value, u.login, u.password, u.roleId.value)
  }

  def spec =
    suite("UserDaoSpec")(
      test("successfully get user")(
        for {
          container <- ZIO.service[PostgreSQLContainer]
          context = new Context(container)
          dao     = context.userDao
          _ <- context.truncate *> context.populate(admin) *> context.populate(reader)

          r <- assertZIO(dao.get(admin.login))(
            equalTo(Some(admin))
          )
        } yield r
      ),
      test("successfully register user")(
        for {
          container <- ZIO.service[PostgreSQLContainer]
          context = new Context(container)
          dao     = context.userDao
          _ <- context.truncate

          r <- assertZIO(dao.add(owner) *> dao.get(owner.login))(
            equalTo(Some(owner))
          )
        } yield r
      ),
      test("successfully edit user's role")(
        for {
          container <- ZIO.service[PostgreSQLContainer]
          context = new Context(container)
          dao     = context.userDao
          _ <- context.truncate *> context.populate(reader)

          newRole = ownerRole

          r <- assertZIO(dao.changeRole(reader.login, newRole.id) *> dao.get(reader.login))(
            equalTo(Some(reader.copy(roleId = newRole.id)))
          )
        } yield r
      ),
      test("successfully delete user")(
        for {
          container <- ZIO.service[PostgreSQLContainer]
          context = new Context(container)
          dao     = context.userDao
          _ <- context.truncate *> context.populate(admin) *> context.populate(reader)

          r <- assertZIO(dao.delete(reader.login) *> dao.get(reader.login))(
            equalTo(None)
          )
        } yield r
      ),
    )
      .provideLayerShared(ZPostgreSQLContainer.live)
      .provide(
        ZPostgreSQLContainer.Settings.default,
      ) @@ TestAspect.sequential @@ TestAspect.timeout(20.seconds)

  private def hashedPassword(pass: String): String = BCrypt.hashpw(pass, BCrypt.gensalt())
}
