package movies.db

import java.util.UUID

import com.dimafeng.testcontainers.PostgreSQLContainer
import doobie.implicits._
import doobie.postgres.implicits._
import io.github.scottweaver.zio.testcontainers.postgres.ZPostgreSQLContainer
import movies.configuration.DbConfig
import movies.db.movie.MovieDaoImpl
import movies.model._
import org.flywaydb.core.Flyway
import zio.Task
import zio.ZIO
import zio.durationInt
import zio.test.Assertion.equalTo
import zio.test._

class MovieDaoSpec extends ZIOSpecDefault {

  val movieT: Movie  = Movie(Movie.Id.random(), "Titanic", Some("love Leo!"), 10)
  val movieGB: Movie = Movie(Movie.Id.random(), "Green Book", None, 10)

  class Context(container: PostgreSQLContainer) {
    import zio.interop.catz.asyncInstance

    Class.forName(container.driverClassName)

    val tm = new TransactorManagerImpl(
      DbConfig(container.driverClassName, container.jdbcUrl, container.username, container.password)
    )
    val transactor = tm.getTransactor
    val movieDao   = new MovieDaoImpl(tm)

    val flywayConfig = Flyway.configure
      .locations("filesystem:./sql/migrations")
      .dataSource(container.jdbcUrl, container.username, container.password)
    val flyway = flywayConfig.load()
    flyway.migrate()

    def truncate = sql"truncate table movie".update.run.transact(transactor)

    def populate(id: UUID, title: String, description: Option[String], rating: Double): Task[Int] =
      sql"INSERT INTO movie (id, title, description, rating) VALUES ($id, $title, $description, $rating)".update.run
        .transact(transactor)

    def populate(m: Movie): Task[Int] = populate(m.id.value, m.title, m.description, m.rating)
  }

  def spec =
    suite("MovieDaoSpec")(
      test("successfully get movies")(
        for {
          container <- ZIO.service[PostgreSQLContainer]
          context = new Context(container)
          dao     = context.movieDao
          _ <- context.truncate *> context.populate(movieT) *> context.populate(movieGB)

          r <- assertZIO(dao.getAll)(
            equalTo(List(movieT, movieGB))
          )
        } yield r
      ),
      test("successfully get movie")(
        for {
          container <- ZIO.service[PostgreSQLContainer]
          context = new Context(container)
          dao     = context.movieDao
          _ <- context.truncate *> context.populate(movieT) *> context.populate(movieGB)

          r <- assertZIO(dao.get(movieT.id))(
            equalTo(Some(movieT))
          )
        } yield r
      ),
      test("successfully add movie")(
        for {
          container <- ZIO.service[PostgreSQLContainer]
          context = new Context(container)
          dao     = context.movieDao
          _ <- context.truncate

          r <- assertZIO(dao.add(movieT) *> dao.get(movieT.id))(
            equalTo(Some(movieT))
          )
        } yield r
      ),
      test("successfully edit movie description")(
        for {
          container <- ZIO.service[PostgreSQLContainer]
          context = new Context(container)
          dao     = context.movieDao
          _ <- context.truncate *> context.populate(movieT)

          newDescription = "love Rose!"

          r <- assertZIO(dao.editDescription(movieT.id, newDescription) *> dao.get(movieT.id))(
            equalTo(Some(movieT.copy(description = Some(newDescription))))
          )
        } yield r
      ),
      test("successfully edit movie rating")(
        for {
          container <- ZIO.service[PostgreSQLContainer]
          context = new Context(container)
          dao     = context.movieDao
          _ <- context.truncate *> context.populate(movieT)

          newRating = 5

          r <- assertZIO(dao.editRating(movieT.id, newRating) *> dao.get(movieT.id))(
            equalTo(Some(movieT.copy(rating = newRating)))
          )
        } yield r
      ),
      test("successfully delete movie")(
        for {
          container <- ZIO.service[PostgreSQLContainer]
          context = new Context(container)
          dao     = context.movieDao
          _ <- context.truncate *> context.populate(movieT) *> context.populate(movieGB)

          r <- assertZIO(dao.delete(movieT.id) *> dao.getAll)(
            equalTo(List(movieGB))
          )
        } yield r
      ),
    )
      .provideLayerShared(ZPostgreSQLContainer.live)
      .provide(
        ZPostgreSQLContainer.Settings.default,
      ) @@ TestAspect.sequential @@ TestAspect.timeout(20.seconds)
}
