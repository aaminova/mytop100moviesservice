package movies.db.mock

import movies.db.role.RoleDao
import movies.model._
import org.mockito.ArgumentMatchers.any
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar.mock
import zio.ZIO

object RoleDaoMocks {
  def roleDaoMocked = mock[RoleDao]

  def getById(mock: RoleDao, res: Option[Role]) =
    when(mock.getById(any())).thenReturn(ZIO.succeed(res))

  def getByName(mock: RoleDao, res: Option[Role]) =
    when(mock.getByName(any())).thenReturn(ZIO.succeed(res))
}
