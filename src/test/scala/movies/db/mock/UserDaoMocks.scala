package movies.db.mock

import movies.db.user.UserDao
import movies.model._
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar.mock
import zio.ZIO

object UserDaoMocks {
  def userDaoMocked = mock[UserDao]

  def get(mock: UserDao, res: Option[User]) =
    when(mock.get(anyString())).thenReturn(ZIO.succeed(res))

  def add(mock: UserDao) =
    when(mock.add(any())).thenReturn(ZIO.unit)

  def delete(mock: UserDao) =
    when(mock.delete(anyString())).thenReturn(ZIO.unit)

  def changeRole(mock: UserDao) =
    when(mock.changeRole(anyString(), any())).thenReturn(ZIO.unit)
}
