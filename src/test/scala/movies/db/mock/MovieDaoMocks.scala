package movies.db.mock

import movies.db.movie.MovieDao
import movies.model._
import org.mockito.ArgumentMatchers.any
import org.mockito.ArgumentMatchers.anyDouble
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar.mock
import zio.ZIO

object MovieDaoMocks {
  def movieDaoMocked = mock[MovieDao]

  def addMovie(mock: MovieDao) =
    when(mock.add(any())).thenReturn(ZIO.unit)

  def getMovie(mock: MovieDao, res: Option[Movie]) =
    when(mock.get(Movie.Id(any()))).thenReturn(ZIO.succeed(res))

  def getMovies(mock: MovieDao, res: List[Movie]) =
    when(mock.getAll).thenReturn(ZIO.succeed(res))

  def editMovieRating(mock: MovieDao) =
    when(mock.editRating(any(), anyDouble())).thenReturn(ZIO.unit)

  def editMovieDescription(mock: MovieDao) =
    when(mock.editDescription(any(), anyString())).thenReturn(ZIO.unit)

  def deleteMovie(mock: MovieDao) =
    when(mock.delete(any())).thenReturn(ZIO.unit)
}
