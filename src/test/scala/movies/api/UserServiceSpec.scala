package movies.api

import io.circe.parser.decode
import movies.Samples._
import movies.db.mock.RoleDaoMocks
import movies.db.mock.UserDaoMocks
import movies.logic.AuthenticationServiceImpl
import movies.logic.AuthorizationServiceImpl
import movies.logic.MyTop100MoviesServiceError._
import movies.logic.UserServiceImpl
import movies.model._
import movies.routing.routes.UserRoute
import movies.routing.routes.model.ChangeRoleRequest
import movies.routing.routes.model.RegisterRequest
import sttp.client3.UriContext
import sttp.client3.basicRequest
import sttp.client3.circe._
import sttp.client3.testing.SttpBackendStub
import sttp.tapir.server.stub.TapirStubInterpreter
import zio.ZIO
import zio.test.Assertion.equalTo
import zio.test._

object UserServiceSpec extends ZIOSpecDefault {
  val interpreter = TapirStubInterpreter(SttpBackendStub.asynchronousFuture)

  def spec =
    suite("UserServiceSpec")(
      suite("CRUD tests")(
        test("successfully register user") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(admin))
          UserDaoMocks.get(userDaoMocked, None)
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole))
          RoleDaoMocks.getByName(roleDaoMocked, Some(readerRole))
          UserDaoMocks.add(userDaoMocked)

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(0))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080.com/api/myTop100Movies/v1/users/register")
              .auth
              .basic(admin.login, adminPass)
              .body(RegisterRequest(reader.login, readerPass))

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[String](_)))(ec)))(
              equalTo(Right("OK"))
            )
          } yield r
        },
        test("can't register existed user") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(admin))
          UserDaoMocks.get(userDaoMocked, Some(reader))
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole))
          RoleDaoMocks.getByName(roleDaoMocked, Some(readerRole))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(0))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080.com/api/myTop100Movies/v1/users/register")
              .auth
              .basic(admin.login, adminPass)
              .body(RegisterRequest(reader.login, readerPass))

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[BadRequest](_)))(ec)))(
              equalTo(Right(BadRequest("Пользователь с таким логином уже существует")))
            )
          } yield r
        },
        test("successfully get user") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(admin))  // authentication
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole)) // authorization
          UserDaoMocks.get(userDaoMocked, Some(reader))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(1))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .get(uri"http://localhost:8080/api/myTop100Movies/v1/users/${reader.login}")
              .auth
              .basic(admin.login, adminPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[User](_)))(ec)))(
              equalTo(Right(reader))
            )
          } yield r
        },
        test("can't get unknown user") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(admin))  // authentication
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole)) // authorization
          UserDaoMocks.get(userDaoMocked, None)

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(1))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .get(uri"http://localhost:8080/api/myTop100Movies/v1/users/${reader.login}")
              .auth
              .basic(admin.login, adminPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[NotFound](_)))(ec)))(
              equalTo(Right(NotFound.default))
            )
          } yield r
        },
        test("successfully change role") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(admin))  // authentication
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole)) // authorization
          UserDaoMocks.get(userDaoMocked, Some(reader))
          RoleDaoMocks.getByName(roleDaoMocked, Some(ownerRole))
          UserDaoMocks.changeRole(userDaoMocked)

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(2))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080/api/myTop100Movies/v1/users/${reader.id}/role")
              .auth
              .basic(admin.login, adminPass)
              .body(ChangeRoleRequest(Role.Type.Owner))

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[String](_)))(ec)))(
              equalTo(Right("Права доступа изменены"))
            )
          } yield r
        },
        test("can't change unknown role") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(admin))  // authentication
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole)) // authorization
          UserDaoMocks.get(userDaoMocked, Some(reader))
          RoleDaoMocks.getByName(roleDaoMocked, None)

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(2))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080/api/myTop100Movies/v1/users/${reader.id}/role")
              .auth
              .basic(admin.login, adminPass)
              .body(ChangeRoleRequest(Role.Type.Owner))

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[NotFound](_)))(ec)))(
              equalTo(Right(NotFound("Роль не найдена")))
            )
          } yield r
        },
        test("successfully delete user") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(admin))  // authentication
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole)) // authorization
          UserDaoMocks.get(userDaoMocked, Some(reader))
          UserDaoMocks.delete(userDaoMocked)

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(3))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .delete(uri"http://localhost:8080/api/myTop100Movies/v1/users/${reader.id}")
              .auth
              .basic(admin.login, adminPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[String](_)))(ec)))(
              equalTo(Right("Пользователь удален"))
            )
          } yield r
        },
        test("can't delete unknown user") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(admin))  // authentication
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole)) // authorization
          UserDaoMocks.get(userDaoMocked, None)

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(3))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .delete(uri"http://localhost:8080/api/myTop100Movies/v1/users/${reader.id}")
              .auth
              .basic(admin.login, adminPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[NotFound](_)))(ec)))(
              equalTo(Right(NotFound("Пользователь с таким логином не зарегистрирован")))
            )
          } yield r
        }
      ),
      suite("RBAC tests")(
        test("can't register user by owner") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(owner))
          RoleDaoMocks.getById(roleDaoMocked, Some(ownerRole))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(0))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080.com/api/myTop100Movies/v1/users/register")
              .auth
              .basic(owner.login, ownerPass)
              .body(RegisterRequest(reader.login, readerPass))

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
              equalTo(Right(Unauthorized("Недостаточно прав!")))
            )
          } yield r
        },
        test("can't register user by reader") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(reader))
          RoleDaoMocks.getById(roleDaoMocked, Some(readerRole))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(0))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080.com/api/myTop100Movies/v1/users/register")
              .auth
              .basic(reader.login, readerPass)
              .body(RegisterRequest(owner.login, ownerPass))

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
              equalTo(Right(Unauthorized("Недостаточно прав!")))
            )
          } yield r
        },
        test("can't get user by owner") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(owner))
          RoleDaoMocks.getById(roleDaoMocked, Some(ownerRole))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(1))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .get(uri"http://localhost:8080/api/myTop100Movies/v1/users/${reader.login}")
              .auth
              .basic(owner.login, ownerPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
              equalTo(Right(Unauthorized("Недостаточно прав!")))
            )
          } yield r
        },
        test("can't get user by reader") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(reader))
          RoleDaoMocks.getById(roleDaoMocked, Some(readerRole))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(1))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .get(uri"http://localhost:8080/api/myTop100Movies/v1/users/${owner.login}")
              .auth
              .basic(reader.login, readerPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
              equalTo(Right(Unauthorized("Недостаточно прав!")))
            )
          } yield r
        },
        test("can't change role by owner") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(owner))  // authentication
          RoleDaoMocks.getById(roleDaoMocked, Some(ownerRole)) // authorization

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(2))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080/api/myTop100Movies/v1/users/${reader.id}/role")
              .auth
              .basic(owner.login, ownerPass)
              .body(ChangeRoleRequest(Role.Type.Owner))

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
              equalTo(Right(Unauthorized("Недостаточно прав!")))
            )
          } yield r
        },
        test("can't change role by reader") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(reader))  // authentication
          RoleDaoMocks.getById(roleDaoMocked, Some(readerRole)) // authorization

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(2))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080/api/myTop100Movies/v1/users/${owner.id}/role")
              .auth
              .basic(reader.login, readerPass)
              .body(ChangeRoleRequest(Role.Type.Reader))

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
              equalTo(Right(Unauthorized("Недостаточно прав!")))
            )
          } yield r
        },
        test("can't delete user by owner") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(owner))  // authentication
          RoleDaoMocks.getById(roleDaoMocked, Some(ownerRole)) // authorization

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(3))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .delete(uri"http://localhost:8080/api/myTop100Movies/v1/users/${reader.id}")
              .auth
              .basic(owner.login, ownerPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
              equalTo(Right(Unauthorized("Недостаточно прав!")))
            )
          } yield r
        },
        test("can't delete user by reader") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(reader))  // authentication
          RoleDaoMocks.getById(roleDaoMocked, Some(readerRole)) // authorization

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(3))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .delete(uri"http://localhost:8080/api/myTop100Movies/v1/users/${owner.id}")
              .auth
              .basic(reader.login, readerPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
              equalTo(Right(Unauthorized("Недостаточно прав!")))
            )
          } yield r
        }
      ),
      suite("Basic auth tests")(
        test("failed basic auth with wrong pass") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, Some(admin))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(0))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080.com/api/myTop100Movies/v1/users/register")
              .auth
              .basic(admin.login, "not_an_admin_pass")
              .body(RegisterRequest(reader.login, readerPass))

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
              equalTo(Right(Unauthorized("Пользователь не найден или пароль не верен")))
            )
          } yield r
        },
        test("failed basic auth with unknown user") {
          val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
          val userDaoMocked        = UserDaoMocks.userDaoMocked
          val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

          UserDaoMocks.get(userDaoMockedForAuth, None)

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
            api                  = new UserRoute(authService, userService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(0))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080.com/api/myTop100Movies/v1/users/register")
              .auth
              .basic("unknown_login", "unknown_pass")
              .body(RegisterRequest(reader.login, readerPass))

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
              equalTo(Right(Unauthorized("Пользователь не найден или пароль не верен")))
            )
          } yield r
        }
      )
    )
}
