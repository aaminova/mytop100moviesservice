package movies.api

import io.circe.parser.decode
import movies.Samples._
import movies.db.mock.MovieDaoMocks
import movies.db.mock.RoleDaoMocks
import movies.db.mock.UserDaoMocks
import movies.logic.AuthenticationServiceImpl
import movies.logic.AuthorizationServiceImpl
import movies.logic.MyTop100MoviesServiceError._
import movies.logic.MyTop100MoviesServiceImpl
import movies.model._
import movies.routing.routes.MovieRoute
import sttp.client3.UriContext
import sttp.client3.basicRequest
import sttp.client3.circe._
import sttp.client3.testing.SttpBackendStub
import sttp.tapir.server.stub.TapirStubInterpreter
import zio.ZIO
import zio.test.Assertion.equalTo
import zio.test._

object MovieSpec extends ZIOSpecDefault {

  val movieT  = Movie(Movie.Id.random(), "Titanic", Some("love Leo!"), 10)
  val movieGB = Movie(Movie.Id.random(), "Green Book", None, 10)

  val interpreter = TapirStubInterpreter(SttpBackendStub.asynchronousFuture)

  def spec =
    suite("MyTop100MoviesService")(
      suite("CRUD tests")(
        test("successfully added movie") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovie(movieDaoMocked, None)
          MovieDaoMocks.addMovie(movieDaoMocked)
          RoleDaoMocks.getById(roleDaoMocked, Some(ownerRole))
          UserDaoMocks.get(userDaoMocked, Some(owner))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(0))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080.com/api/myTop100Movies/v1/movies/add")
              .auth
              .basic(owner.login, ownerPass)
              .body(Movie(Movie.Id.random(), "anotherMovie", None, 0))

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[String](_)))(ec)))(
              equalTo(Right("OK"))
            )
          } yield r

        },
        test("successfully get movies") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovies(movieDaoMocked, List(movieT, movieGB))
          RoleDaoMocks.getById(roleDaoMocked, Some(ownerRole))
          UserDaoMocks.get(userDaoMocked, Some(owner))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(1))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .get(uri"http://localhost:8080/api/myTop100Movies/v1/movies/all")
              .auth
              .basic(owner.login, ownerPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[List[Movie]](_)))(ec)))(
              equalTo(Right(List(movieT, movieGB)))
            )
          } yield r
        },
        test("successfully get movie") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovie(movieDaoMocked, Some(movieT))
          RoleDaoMocks.getById(roleDaoMocked, Some(ownerRole))
          UserDaoMocks.get(userDaoMocked, Some(owner))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(2))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .get(uri"http://localhost:8080/api/myTop100Movies/v1/movies/${movieT.id}")
              .auth
              .basic(owner.login, ownerPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[Movie](_)))(ec)))(
              equalTo(Right(movieT))
            )
          } yield r

        },
        test("successfully edit movie rating") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovie(movieDaoMocked, Some(movieGB))
          MovieDaoMocks.editMovieRating(movieDaoMocked)
          RoleDaoMocks.getById(roleDaoMocked, Some(ownerRole))
          UserDaoMocks.get(userDaoMocked, Some(owner))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(4))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080/api/myTop100Movies/v1/movies/${movieGB.id}/rating")
              .auth
              .basic(owner.login, ownerPass)
              .body(5.0)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[String](_)))(ec)))(
              equalTo(Right("Рейтинг фильма успешно изменен"))
            )
          } yield r

        },
        test("can't edit not existed movie") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovie(movieDaoMocked, None)
          RoleDaoMocks.getById(roleDaoMocked, Some(ownerRole))
          UserDaoMocks.get(userDaoMocked, Some(owner))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(3))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080/api/myTop100Movies/v1/movies/${Movie.Id.random()}/description")
              .auth
              .basic(owner.login, ownerPass)
              .body("\"some description\"")

            response = request.send(backendStub)
            r <- assertZIO(
              ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[BadRequest](_)).swap)(ec))
            )(
              equalTo(Left(BadRequest("Фильма с таким id нет в списке")))
            )
          } yield r
        },
        test("can delete movie") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovie(movieDaoMocked, Some(movieGB))
          MovieDaoMocks.deleteMovie(movieDaoMocked)
          RoleDaoMocks.getById(roleDaoMocked, Some(ownerRole))
          UserDaoMocks.get(userDaoMocked, Some(owner))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(5))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .delete(uri"http://localhost:8080/api/myTop100Movies/v1/movies/${movieGB.id}")
              .auth
              .basic(owner.login, ownerPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[String](_)))(ec)))(
              equalTo(Right("OK"))
            )
          } yield r
        }
      ),
      suite("RBAC tests")(
        test("successfully added movie by admin") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovie(movieDaoMocked, None)
          MovieDaoMocks.addMovie(movieDaoMocked)
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole))
          UserDaoMocks.get(userDaoMocked, Some(admin))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(0))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080.com/api/myTop100Movies/v1/movies/add")
              .auth
              .basic(admin.login, adminPass)
              .body(Movie(Movie.Id.random(), "anotherMovie", None, 0))

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[String](_)))(ec)))(
              equalTo(Right("OK"))
            )
          } yield r
        },
        test("can't add movie by reader") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          RoleDaoMocks.getById(roleDaoMocked, Some(readerRole))
          UserDaoMocks.get(userDaoMocked, Some(reader))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(0))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080.com/api/myTop100Movies/v1/movies/add")
              .auth
              .basic(reader.login, readerPass)
              .body(Movie(Movie.Id.random(), "anotherMovie", None, 0))

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
              equalTo(Right(Unauthorized("Недостаточно прав!")))
            )
          } yield r
        },
        test("successfully get movies by admin") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovies(movieDaoMocked, List(movieT, movieGB))
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole))
          UserDaoMocks.get(userDaoMocked, Some(admin))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(1))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .get(uri"http://localhost:8080/api/myTop100Movies/v1/movies/all")
              .auth
              .basic(admin.login, adminPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[List[Movie]](_)))(ec)))(
              equalTo(Right(List(movieT, movieGB)))
            )
          } yield r
        },
        test("successfully get movies by reader") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovies(movieDaoMocked, List(movieT, movieGB))
          RoleDaoMocks.getById(roleDaoMocked, Some(readerRole))
          UserDaoMocks.get(userDaoMocked, Some(reader))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(1))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .get(uri"http://localhost:8080/api/myTop100Movies/v1/movies/all")
              .auth
              .basic(reader.login, readerPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[List[Movie]](_)))(ec)))(
              equalTo(Right(List(movieT, movieGB)))
            )
          } yield r
        },
        test("successfully get movie by admin") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovie(movieDaoMocked, Some(movieT))
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole))
          UserDaoMocks.get(userDaoMocked, Some(admin))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(2))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .get(uri"http://localhost:8080/api/myTop100Movies/v1/movies/${movieT.id}")
              .auth
              .basic(admin.login, adminPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[Movie](_)))(ec)))(
              equalTo(Right(movieT))
            )
          } yield r
        },
        test("successfully get movie by reader") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovie(movieDaoMocked, Some(movieT))
          RoleDaoMocks.getById(roleDaoMocked, Some(readerRole))
          UserDaoMocks.get(userDaoMocked, Some(reader))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(2))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .get(uri"http://localhost:8080/api/myTop100Movies/v1/movies/${movieT.id}")
              .auth
              .basic(reader.login, readerPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[Movie](_)))(ec)))(
              equalTo(Right(movieT))
            )
          } yield r
        },
        test("successfully edit movie rating by admin") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovie(movieDaoMocked, Some(movieGB))
          MovieDaoMocks.editMovieRating(movieDaoMocked)
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole))
          UserDaoMocks.get(userDaoMocked, Some(admin))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(4))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080/api/myTop100Movies/v1/movies/${movieGB.id}/rating")
              .auth
              .basic(admin.login, adminPass)
              .body(5.0)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[String](_)))(ec)))(
              equalTo(Right("Рейтинг фильма успешно изменен"))
            )
          } yield r
        },
        test("can't edit movie rating by reader") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          RoleDaoMocks.getById(roleDaoMocked, Some(readerRole))
          UserDaoMocks.get(userDaoMocked, Some(reader))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(4))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080/api/myTop100Movies/v1/movies/${movieGB.id}/rating")
              .auth
              .basic(reader.login, readerPass)
              .body(5.0)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
              equalTo(Right(Unauthorized("Недостаточно прав!")))
            )
          } yield r
        },
        test("successfully edit movie description by admin") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovie(movieDaoMocked, Some(movieGB))
          MovieDaoMocks.editMovieDescription(movieDaoMocked)
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole))
          UserDaoMocks.get(userDaoMocked, Some(admin))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(3))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080/api/myTop100Movies/v1/movies/${Movie.Id.random()}/description")
              .auth
              .basic(admin.login, adminPass)
              .body("\"some description\"")

            response = request.send(backendStub)
            r <- assertZIO(
              ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[String](_)))(ec))
            )(
              equalTo(Right("Описание фильма успешно изменено"))
            )
          } yield r
        },
        test("can't edit movie description by reader") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          RoleDaoMocks.getById(roleDaoMocked, Some(readerRole))
          UserDaoMocks.get(userDaoMocked, Some(reader))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(3))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .post(uri"http://localhost:8080/api/myTop100Movies/v1/movies/${Movie.Id.random()}/description")
              .auth
              .basic(reader.login, readerPass)
              .body("\"some description\"")

            response = request.send(backendStub)
            r <- assertZIO(
              ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec))
            )(
              equalTo(Right(Unauthorized("Недостаточно прав!")))
            )
          } yield r
        },
        test("can delete movie by admin") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          MovieDaoMocks.getMovie(movieDaoMocked, Some(movieGB))
          MovieDaoMocks.deleteMovie(movieDaoMocked)
          RoleDaoMocks.getById(roleDaoMocked, Some(adminRole))
          UserDaoMocks.get(userDaoMocked, Some(admin))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(5))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .delete(uri"http://localhost:8080/api/myTop100Movies/v1/movies/${movieGB.id}")
              .auth
              .basic(admin.login, adminPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.flatMap(decode[String](_)))(ec)))(
              equalTo(Right("OK"))
            )
          } yield r
        },
        test("can't delete movie by reader") {
          val movieDaoMocked = MovieDaoMocks.movieDaoMocked
          val userDaoMocked  = UserDaoMocks.userDaoMocked
          val roleDaoMocked  = RoleDaoMocks.roleDaoMocked

          RoleDaoMocks.getById(roleDaoMocked, Some(readerRole))
          UserDaoMocks.get(userDaoMocked, Some(reader))

          for {
            runtime <- ZIO.runtime[Any]
            authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
            service              = MyTop100MoviesServiceImpl(movieDaoMocked, authorizationService)
            authService          = new AuthenticationServiceImpl(userDaoMocked)
            api                  = new MovieRoute(service, authService)(runtime)
            backendStub = interpreter
              .whenServerEndpoint(api.routes(5))
              .thenRunLogic()
              .backend()

            request = basicRequest
              .delete(uri"http://localhost:8080/api/myTop100Movies/v1/movies/${movieGB.id}")
              .auth
              .basic(reader.login, readerPass)

            response = request.send(backendStub)
            r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
              equalTo(Right(Unauthorized("Недостаточно прав!")))
            )
          } yield r
        }
      )
    )
}
