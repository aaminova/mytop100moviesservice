package movies

import movies.model._
import org.mindrot.jbcrypt.BCrypt

object Samples {
  val adminPass  = "admin_pass"
  val ownerPass  = "owner_pass"
  val readerPass = "reader_pass"

  val adminRole  = Role(Role.Id(1), Role.Type.Admin.toText)
  val ownerRole  = Role(Role.Id(2), Role.Type.Owner.toText)
  val readerRole = Role(Role.Id(3), Role.Type.Reader.toText)

  val admin: User  = User(User.Id.random(), "god", hashedPassword(adminPass), adminRole.id)
  val owner: User  = User(User.Id.random(), "blogger", hashedPassword(ownerPass), ownerRole.id)
  val reader: User = User(User.Id.random(), "mimo_crocodil", hashedPassword(readerPass), readerRole.id)

  private def hashedPassword(pass: String): String = BCrypt.hashpw(pass, BCrypt.gensalt())
}
