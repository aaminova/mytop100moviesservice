package movies.authentication

import io.circe.parser.decode
import movies.Samples._
import movies.db.mock._
import movies.logic.AuthenticationServiceImpl
import movies.logic.AuthorizationServiceImpl
import movies.logic.MyTop100MoviesServiceError.Unauthorized
import movies.logic.UserServiceImpl
import movies.routing.routes.UserRoute
import movies.routing.routes.model.RegisterRequest
import sttp.client3.UriContext
import sttp.client3.basicRequest
import sttp.client3.circe._
import sttp.client3.testing.SttpBackendStub
import sttp.tapir.server.stub.TapirStubInterpreter
import zio.ZIO
import zio.test.Assertion.equalTo
import zio.test._

object AuthenticationServiceSpec extends ZIOSpecDefault {
  val interpreter = TapirStubInterpreter(SttpBackendStub.asynchronousFuture)

  def spec = suite("Basic auth tests")(
    test("failed basic auth with wrong pass") {
      val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
      val userDaoMocked        = UserDaoMocks.userDaoMocked
      val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

      UserDaoMocks.get(userDaoMockedForAuth, Some(admin))

      for {
        runtime <- ZIO.runtime[Any]
        authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
        userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
        authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
        api                  = new UserRoute(authService, userService)(runtime)
        backendStub = interpreter
          .whenServerEndpoint(api.routes(0))
          .thenRunLogic()
          .backend()

        request = basicRequest
          .post(uri"http://localhost:8080.com/api/myTop100Movies/v1/users/register")
          .auth
          .basic(admin.login, "not_an_admin_pass")
          .body(RegisterRequest(reader.login, readerPass))

        response = request.send(backendStub)
        r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
          equalTo(Right(Unauthorized("Пользователь не найден или пароль не верен")))
        )
      } yield r
    },
    test("failed basic auth with unknown user") {
      val userDaoMockedForAuth = UserDaoMocks.userDaoMocked
      val userDaoMocked        = UserDaoMocks.userDaoMocked
      val roleDaoMocked        = RoleDaoMocks.roleDaoMocked

      UserDaoMocks.get(userDaoMockedForAuth, None)

      for {
        runtime <- ZIO.runtime[Any]
        authorizationService = new AuthorizationServiceImpl(roleDaoMocked)
        userService          = UserServiceImpl(userDaoMocked, roleDaoMocked, authorizationService)
        authService          = new AuthenticationServiceImpl(userDaoMockedForAuth)
        api                  = new UserRoute(authService, userService)(runtime)
        backendStub = interpreter
          .whenServerEndpoint(api.routes(0))
          .thenRunLogic()
          .backend()

        request = basicRequest
          .post(uri"http://localhost:8080.com/api/myTop100Movies/v1/users/register")
          .auth
          .basic("unknown_login", "unknown_pass")
          .body(RegisterRequest(reader.login, readerPass))

        response = request.send(backendStub)
        r <- assertZIO(ZIO.fromFuture(ec => response.map(_.body.swap.flatMap(decode[Unauthorized](_)))(ec)))(
          equalTo(Right(Unauthorized("Пользователь не найден или пароль не верен")))
        )
      } yield r
    }
  )
}
