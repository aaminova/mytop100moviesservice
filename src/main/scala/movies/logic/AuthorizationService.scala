package movies.logic

import cats.syntax.order._
import movies.db.role.RoleDao
import movies.logic.MyTop100MoviesServiceError.Unauthorized
import movies.model.Role
import movies.model.User
import tofu.syntax.loggable._
import zio._

trait AuthorizationService {
  def withRoleAuth[T](role: Role.Type)(user: User)(
      action: User => Task[Either[MyTop100MoviesServiceError, T]]
  ): Task[Either[MyTop100MoviesServiceError, T]]
}

final class AuthorizationServiceImpl(roleDao: RoleDao) extends AuthorizationService {
  override def withRoleAuth[T](roleExpected: Role.Type)(user: User)(
      action: User => Task[
        Either[MyTop100MoviesServiceError, T]
      ]
  ): Task[Either[MyTop100MoviesServiceError, T]] =
    roleDao.getById(user.roleId).flatMap {
      case Some(role) if role.name.toType >= roleExpected => action(user)
      case Some(_)                                        => ZIO.left(Unauthorized("Недостаточно прав!"))
      case None =>
        ZIO.logError("Неизвестная роль") @@ ZIOAspect.annotated("roleId" -> user.roleId.logShow) *> ZIO.left(
          Unauthorized("Недостаточно прав!")
        )
    }
}

object AuthorizationService {
  val live: RLayer[RoleDao, AuthorizationService] = ZLayer.fromFunction(new AuthorizationServiceImpl(_))
}
