package movies.logic

import cats.syntax.either._
import movies.db.movie.MovieDao
import movies.logic.MyTop100MoviesServiceError._
import movies.model._
import tofu.syntax.loggable.TofuLoggableOps
import zio._

trait MyTop100MoviesService {
  def addMovie(cred: User, body: Movie): Task[Either[MyTop100MoviesServiceError, String]]
  def getMovies(cred: User): Task[Either[MyTop100MoviesServiceError, List[Movie]]] // TODO: добавить сортировку списка (по времени добавления, по рейтингу, по режиссёру?)
  def getMovie(cred: User, id: Movie.Id): Task[Either[MyTop100MoviesServiceError, Movie]]
  def editMovieDescription(
      user: User,
      id: Movie.Id,
      description: String
  ): Task[Either[MyTop100MoviesServiceError, String]]
  def editMovieRating(cred: User, id: Movie.Id, rating: Double): Task[Either[MyTop100MoviesServiceError, String]]
  def deleteMovie(cred: User, id: Movie.Id): Task[Either[MyTop100MoviesServiceError, String]]
}

final case class MyTop100MoviesServiceImpl(db: MovieDao, authorizationService: AuthorizationService)
    extends MyTop100MoviesService {
  private def withReadAuth[T](user: User)(
      action: User => Task[Either[MyTop100MoviesServiceError, T]]
  ): Task[Either[MyTop100MoviesServiceError, T]] =
    authorizationService.withRoleAuth(Role.Type.Reader)(user)(action)

  private def withOwnerAuth[T](user: User)(
      action: User => Task[Either[MyTop100MoviesServiceError, T]]
  ): Task[Either[MyTop100MoviesServiceError, T]] =
    authorizationService.withRoleAuth(Role.Type.Owner)(user)(action)

  private def findMovieOrFail(id: Movie.Id) =
    for {
      maybeMovie <- db.get(id)
      movie <- maybeMovie.fold[Task[Movie]](
        ZIO.fail(new NoSuchElementException("Фильма с таким id нет в списке"))
          <* ZIO.logInfo("Фильма нет в списке") @@ ZIOAspect.annotated("movieId" -> id.toString)
      )(ZIO.succeed(_))
    } yield movie

  override def addMovie(cred: User, body: Movie): Task[Either[MyTop100MoviesServiceError, String]] =
    withOwnerAuth(cred) { user =>
      for {
        _ <- ZIO.logInfo("Запрос на добавление фильма") @@ ZIOAspect.annotated(
          "movieId" -> body.id.toString,
          "userId"  -> user.id.toString,
          "payload" -> body.logShow
        )
        isMovieExist <- db.get(body.id)
        r <- isMovieExist.fold(
          db.add(body).map(_.asRight[MyTop100MoviesServiceError]) <* ZIO.logInfo("Фильм добавлен")
        )(_ =>
          ZIO.left(BadRequest("Фильм с таким названием уже существует")) <* ZIO.logInfo(
            s"Фильм с названием ${body.title} уже существует"
          )
        )
      } yield r.map(_ => "OK")
    }
      .catchAll(e => ZIO.logError(e.getMessage) *> ZIO.left(InternalError(e.getMessage)))

  override def getMovies(cred: User): Task[Either[MyTop100MoviesServiceError, List[Movie]]] =
    withReadAuth(cred) { user =>
      for {
        _      <- ZIO.logInfo("Запрос списка всех фильмов") @@ ZIOAspect.annotated("userId" -> user.id.toString)
        movies <- db.getAll
      } yield movies.asRight
    }
      .catchAll(e => ZIO.logError(e.getMessage) *> ZIO.left(InternalError(e.getMessage)))

  override def getMovie(cred: User, id: Movie.Id): Task[Either[MyTop100MoviesServiceError, Movie]] =
    withReadAuth(cred) { user =>
      for {
        _ <- ZIO.logInfo("Запрос фильма") @@ ZIOAspect.annotated("movieId" -> id.toString, "userId" -> user.id.toString)
        maybeMovie <- db.get(id)
        r <- maybeMovie.fold(
          ZIO.left(BadRequest(s"Фильм `$id` не найден")): Task[Either[MyTop100MoviesServiceError, Movie]]
        )(ZIO.right(_))
      } yield r
    }
      .catchAll(e => ZIO.logError(e.getMessage) *> ZIO.left(InternalError(e.getMessage)))

  override def editMovieDescription(
      cred: User,
      id: Movie.Id,
      description: String
  ): Task[Either[MyTop100MoviesServiceError, String]] =
    withOwnerAuth(cred) { user =>
      for {
        _ <- ZIO.logInfo("Запрос на изменение описания фильма") @@ ZIOAspect.annotated(
          "movieId" -> id.toString,
          "userId"  -> user.id.toString
        )
        _ <- findMovieOrFail(id)
        _ <- db.editDescription(id, description)
        _ <- ZIO.logInfo("Описание фильма изменено") @@ ZIOAspect.annotated(
          "movieId" -> id.toString,
          "userId"  -> user.id.toString
        )
      } yield "Описание фильма успешно изменено".asRight[MyTop100MoviesServiceError]
    }
      .catchAll {
        case e: NoSuchElementException => ZIO.left(NotFound(e.getMessage))
        case e                         => ZIO.logError(e.getMessage) *> ZIO.left(InternalError(e.getMessage))
      }

  override def editMovieRating(
      cred: User,
      id: Movie.Id,
      rating: Double
  ): Task[Either[MyTop100MoviesServiceError, String]] =
    withOwnerAuth(cred) { user =>
      for {
        _ <- ZIO.logInfo("Запрос на изменение рейтинга фильма") @@ ZIOAspect.annotated(
          "movieId" -> id.toString,
          "userId"  -> user.id.toString
        )
        _ <- findMovieOrFail(id)
        _ <- db.editRating(id, rating)
        _ <- ZIO.logInfo("Рейтинг фильма успешно изменен") @@ ZIOAspect.annotated(
          "movieId" -> id.toString,
          "userId"  -> user.id.toString
        )
      } yield "Рейтинг фильма успешно изменен".asRight[MyTop100MoviesServiceError]
    }
      .catchAll {
        case e: NoSuchElementException => ZIO.left(NotFound(e.getMessage))
        case e                         => ZIO.logError(e.getMessage) *> ZIO.left(InternalError(e.getMessage))
      }

  override def deleteMovie(
      cred: User,
      id: Movie.Id
  ): Task[Either[MyTop100MoviesServiceError, String]] =
    withOwnerAuth(cred) { user =>
      for {
        _ <- ZIO.logInfo("Запрос на удаление фильма") @@ ZIOAspect.annotated(
          "movieId" -> id.toString,
          "userId"  -> user.id.toString
        )
        _ <- findMovieOrFail(id)
        _ <- db.delete(id)
        _ <- ZIO.logInfo("Фильм удален") @@ ZIOAspect.annotated("movieId" -> id.toString, "userId" -> user.id.toString)
      } yield "OK".asRight[MyTop100MoviesServiceError]
    }
      .catchAll {
        case e: NoSuchElementException => ZIO.left(NotFound(e.getMessage))
        case e                         => ZIO.logError(e.getMessage) *> ZIO.left(InternalError(e.getMessage))
      }
}

object MyTop100MoviesService {
  val live: RLayer[MovieDao & AuthorizationService, MyTop100MoviesService] =
    ZLayer.fromFunction(MyTop100MoviesServiceImpl.apply _)
}
