package movies.logic

import movies.db.user.UserDao
import movies.logic.MyTop100MoviesServiceError.Unauthorized
import movies.model.User
import org.mindrot.jbcrypt.BCrypt
import sttp.tapir.model.UsernamePassword
import zio.RLayer
import zio.Task
import zio.ZLayer

trait AuthenticationService {
  def securityLogic(cred: UsernamePassword): Task[Either[MyTop100MoviesServiceError, User]]
}

final class AuthenticationServiceImpl(userDao: UserDao) extends AuthenticationService {
  override def securityLogic(cred: UsernamePassword): Task[Either[MyTop100MoviesServiceError, User]] =
    userDao.get(cred.username).map {
      case Some(user) if BCrypt.checkpw(cred.password.getOrElse(""), user.password) => Right(user)
      case _ => Left(Unauthorized("Пользователь не найден или пароль не верен"))
    }
}

object AuthenticationService {
  val live: RLayer[UserDao, AuthenticationService] = ZLayer.fromFunction(new AuthenticationServiceImpl(_))
}
