package movies.logic

import cats.syntax.either._
import movies.db.role.RoleDao
import movies.db.user.UserDao
import movies.logic.MyTop100MoviesServiceError._
import movies.model.Role
import movies.model.User
import movies.routing.routes.model.ChangeRoleRequest
import movies.routing.routes.model.RegisterRequest
import org.mindrot.jbcrypt.BCrypt
import tofu.syntax.loggable._
import zio.&
import zio.RLayer
import zio.Task
import zio.ZIO
import zio.ZIOAspect
import zio.ZLayer

trait UserService {
  def register(cred: User, newUser: RegisterRequest): Task[Either[MyTop100MoviesServiceError, String]]
  def getUser(cred: User, login: String): Task[Either[MyTop100MoviesServiceError, User]]
  def changeRole(cred: User, login: String, req: ChangeRoleRequest): Task[Either[MyTop100MoviesServiceError, String]]
  def deleteUser(cred: User, login: String): Task[Either[MyTop100MoviesServiceError, String]]
}

final case class UserServiceImpl(userDao: UserDao, roleDao: RoleDao, authorizationService: AuthorizationService)
    extends UserService {
  private def withAdminAuth[T](user: User)(
      action: User => Task[Either[MyTop100MoviesServiceError, T]]
  ): Task[Either[MyTop100MoviesServiceError, T]] =
    authorizationService.withRoleAuth(Role.Type.Admin)(user)(action)

  private def findUserOrFail(login: String) =
    for {
      maybeUser <- userDao.get(login)
      user <- maybeUser.fold[Task[User]](
        ZIO.fail(new NoSuchElementException("Пользователь с таким логином не зарегистрирован"))
          <* ZIO.logInfo("Пользователь не найден") @@ ZIOAspect.annotated("login" -> login)
      )(ZIO.succeed(_))
    } yield user

  private def findRoleOrFail(name: Role.Text) =
    for {
      maybeRole <- roleDao.getByName(name)
      role <- maybeRole.fold[Task[Role]](
        ZIO.fail(new NoSuchElementException("Роль не найдена"))
          <* ZIO.logError("Роль не найдена") @@ ZIOAspect.annotated("role" -> name.logShow)
      )(ZIO.succeed(_))
    } yield role

  override def register(
      cred: User,
      newUser: RegisterRequest
  ): Task[Either[MyTop100MoviesServiceError, String]] =
    withAdminAuth(cred) { admin =>
      for {
        _ <- ZIO.logInfo("Запрос на регистрацию пользователя") @@ ZIOAspect.annotated(
          "userId"  -> admin.id.toString,
          "payload" -> newUser.logShow
        )
        isUserExist <- userDao.get(newUser.login)
        readerRole  <- findRoleOrFail(Role.Type.Reader.toText)
        hashedPassword = BCrypt.hashpw(newUser.password, BCrypt.gensalt())
        user           = User(User.Id.random(), newUser.login, hashedPassword, readerRole.id)

        r <- isUserExist.fold(
          userDao.add(user).map(_.asRight[MyTop100MoviesServiceError])
        )(_ =>
          ZIO.left(BadRequest("Пользователь с таким логином уже существует")) <* ZIO.logInfo(
            s"Пользователь с логином ${user.login} уже существует"
          )
        )
      } yield r.map(_ => "OK")
    }.catchAll(e => ZIO.logError(e.getMessage) *> ZIO.left(InternalError(e.getMessage)))

  override def getUser(
      cred: User,
      login: String
  ): Task[Either[MyTop100MoviesServiceError, User]] =
    withAdminAuth(cred) { admin =>
      for {
        _ <- ZIO.logInfo("Запрос информации о пользователе") @@ ZIOAspect.annotated(
          "userId"  -> admin.id.toString,
          "payload" -> login
        )
        maybeUser <- userDao.get(login)
        user = maybeUser.toRight(NotFound.default)
      } yield user
    }.catchAll(e => ZIO.logError(e.getMessage) *> ZIO.left(InternalError(e.getMessage)))

  override def changeRole(
      cred: User,
      login: String,
      req: ChangeRoleRequest
  ): Task[Either[MyTop100MoviesServiceError, String]] =
    withAdminAuth(cred) { admin =>
      for {
        _ <- ZIO.logInfo("Запрос на изменение прав пользователя") @@ ZIOAspect.annotated(
          "userId" -> admin.id.toString,
          "role"   -> req.role.entryName
        )
        _    <- findUserOrFail(login)
        role <- findRoleOrFail(req.role.toText)
        _    <- userDao.changeRole(login, role.id)
      } yield "Права доступа изменены".asRight[MyTop100MoviesServiceError]
    }.catchAll {
      case e: NoSuchElementException => ZIO.left(NotFound(e.getMessage))
      case e                         => ZIO.logError(e.getMessage) *> ZIO.left(InternalError(e.getMessage))
    }

  override def deleteUser(
      cred: User,
      login: String
  ): Task[Either[MyTop100MoviesServiceError, String]] =
    withAdminAuth(cred) { admin =>
      for {
        _ <- ZIO.logInfo("Запрос на удаление пользователя") @@ ZIOAspect.annotated(
          "userId"  -> admin.id.toString,
          "payload" -> login
        )
        _ <- findUserOrFail(login)
        _ <- userDao.delete(login)
      } yield "Пользователь удален".asRight[MyTop100MoviesServiceError]
    }.catchAll {
      case e: NoSuchElementException => ZIO.left(NotFound(e.getMessage))
      case e                         => ZIO.logError(e.getMessage) *> ZIO.left(InternalError(e.getMessage))
    }
}

object UserService {
  val live: RLayer[UserDao & RoleDao & AuthorizationService, UserService] =
    ZLayer.fromFunction(UserServiceImpl.apply _)
}
