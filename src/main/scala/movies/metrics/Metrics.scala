package movies.metrics

import scala.concurrent.Future

import io.prometheus.client.CollectorRegistry
import sttp.tapir.server.metrics.prometheus.PrometheusMetrics
import zio.ZIO
import zio.ZLayer

final case class Metrics(
    collectorRegistry: CollectorRegistry,
    tapirMetrics: PrometheusMetrics[Future]
)

object Metrics {
  val live = ZLayer(
    for {
      collectorRegistry <- ZIO.succeed(CollectorRegistry.defaultRegistry)
      tapirMetrics      <- ZIO.succeed(PrometheusMetrics.default[Future]("http", collectorRegistry))
    } yield Metrics(collectorRegistry, tapirMetrics)
  )
}
