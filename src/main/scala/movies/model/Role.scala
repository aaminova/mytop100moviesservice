package movies.model

import enumeratum.EnumEntry.Lowercase
import enumeratum._
import io.circe.Decoder
import io.circe.Encoder
import io.estatico.newtype.macros.newtype
import io.getquill.MappedEncoding
import sttp.tapir.Codec
import sttp.tapir.CodecFormat.TextPlain
import sttp.tapir.Schema
import tofu.logging.Loggable

final case class Role(id: Role.Id, name: Role.Text)

object Role {
  @newtype case class Id(value: Int) {
    override def toString: String = value.toString
  }

  object Id {

    implicit val encoder: Encoder[Id]                = Encoder[Int].contramap(_.value)
    implicit val decoder: Decoder[Id]                = Decoder[Int].map(Id(_))
    implicit val loggable: Loggable[Id]              = Loggable[Int].contramap(_.value)
    implicit val codec: Codec[String, Id, TextPlain] = Codec.int.map(Id(_))(_.value)
    implicit val schema: Schema[Id]                  = deriving

    implicit val quillEncoder: MappedEncoding[Id, Int] = MappedEncoding(_.value)
    implicit val quillDecoder: MappedEncoding[Int, Id] = MappedEncoding(Id(_))
  }

  @newtype case class Text(value: String) {
    override def toString: String = value

    def toType: Role.Type = value match {
      case "reader" => Type.Reader
      case "owner"  => Type.Owner
      case "admin"  => Type.Admin
    }
  }

  object Text {
    implicit val encoder: Encoder[Text]                = Encoder[String].contramap(_.value)
    implicit val decoder: Decoder[Text]                = Decoder[String].map(Text(_))
    implicit val loggable: Loggable[Text]              = Loggable[String].contramap(_.value)
    implicit val codec: Codec[String, Text, TextPlain] = Codec.string.map(Text(_))(_.value)
    implicit val schema: Schema[Text]                  = deriving

    implicit val quillEncoder: MappedEncoding[Text, String] = MappedEncoding(_.value)
    implicit val quillDecoder: MappedEncoding[String, Text] = MappedEncoding(Text(_))
  }

  sealed abstract class Type(val order: Int) extends EnumEntry {
    def toText: Role.Text = this match {
      case Type.Reader => Text("reader")
      case Type.Owner  => Text("owner")
      case Type.Admin  => Text("admin")
    }
  }

  object Type extends Enum[Type] with Lowercase {
    val values = findValues

    case object Admin extends Type(10) // Может добавлять пользователей
    case object Owner extends Type(2)  // Может создавать/ удалять/ редактировать список
    case object Reader extends Type(1) // Может лишь читать

    implicit val encoder: Encoder[Type] = Encoder.encodeString.contramap(_.entryName)
    implicit val decoder: Decoder[Type] = Decoder.decodeString.emap(s =>
      Type.withNameOption(s) match {
        case Some(role) => Right(role)
        case None       => Left(s"Role $s not found")
      }
    )
    implicit val loggable: Loggable[Type] = Loggable[String].contramap(_.entryName)
    implicit val s: Schema[Type]          = Schema.derived

    implicit val order: cats.Order[Type] = cats.Order.from { case (s1, s2) =>
      if (s1.order < s2.order) -1
      else if (s1.order == s2.order) 0
      else 1
    }
  }
}
