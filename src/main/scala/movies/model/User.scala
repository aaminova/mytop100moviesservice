package movies.model

import java.util.UUID

import derevo.circe.decoder
import derevo.circe.encoder
import derevo.derive
import io.circe.Decoder
import io.circe.Encoder
import io.estatico.newtype.macros.newtype
import io.getquill.MappedEncoding
import sttp.tapir.Codec
import sttp.tapir.CodecFormat.TextPlain
import sttp.tapir.Schema
import sttp.tapir.derevo.schema
import tofu.logging.Loggable
import tofu.logging.derivation.loggable

@derive(encoder, decoder, schema, loggable)
final case class User(
    id: User.Id,
    login: String,
    password: String,
    roleId: Role.Id
)

object User {
  @newtype case class Id(value: UUID) {
    override def toString: String = value.toString
  }

  object Id {

    implicit val encoder: Encoder[Id]                = Encoder[UUID].contramap(_.value)
    implicit val decoder: Decoder[Id]                = Decoder[UUID].map(Id(_))
    implicit val loggable: Loggable[Id]              = Loggable[UUID].contramap(_.value)
    implicit val codec: Codec[String, Id, TextPlain] = Codec.uuid.map(Id(_))(_.value)
    implicit val schema: Schema[Id]                  = deriving

    implicit val quillEncoder: MappedEncoding[Id, UUID] = MappedEncoding(_.value)
    implicit val quillDecoder: MappedEncoding[UUID, Id] = MappedEncoding(Id(_))

    def random(): Id = Id(UUID.randomUUID())
  }
}
