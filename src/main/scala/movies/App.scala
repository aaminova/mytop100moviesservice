package movies

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import com.typesafe.scalalogging.LazyLogging
import movies.configuration._
import movies.db.TransactorManager
import movies.db.movie.MovieDaoLoggingDecorator
import movies.db.role.RoleDaoLoggingDecorator
import movies.db.user.UserDaoLoggingDecorator
import movies.logic.AuthenticationService
import movies.logic.AuthorizationService
import movies.logic.MyTop100MoviesService
import movies.logic.UserService
import movies.metrics.Metrics
import movies.routing.Server
import movies.routing.routes.MovieRoute
import movies.routing.routes.UserRoute
import zio.RLayer
import zio.ZIO
import zio.ZIOAppDefault
import zio.ZLayer

object App extends LazyLogging with ZIOAppDefault {

  val config = AppConfig.layer

  val actorSystem: RLayer[AppConfig, ActorSystem] =
    ZLayer.scoped(
      ZIO.acquireRelease(ZIO.attempt(ActorSystem("default")))(as => ZIO.fromFuture(_ => as.terminate()).unit.orDie)
    )

  val appLayer: RLayer[Any, Http.ServerBinding] =
    ZLayer.make[Http.ServerBinding](
      config,
      config.project(_.api),
      config.project(_.db),
      actorSystem,
      TransactorManager.live,
      MovieDaoLoggingDecorator.live,
      UserDaoLoggingDecorator.live,
      RoleDaoLoggingDecorator.live,
      AuthenticationService.live,
      AuthorizationService.live,
      MyTop100MoviesService.live,
      UserService.live,
      MovieRoute.live,
      UserRoute.live,
      Server.live,
      Metrics.live
    )

  override def run: ZIO[Any, Throwable, Http.ServerBinding] =
    (ZIO.service[Http.ServerBinding] *> ZIO.never).provide(appLayer)
}
