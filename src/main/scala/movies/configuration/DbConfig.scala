package movies.configuration

case class DbConfig(driver: String, url: String, user: String, password: String)
