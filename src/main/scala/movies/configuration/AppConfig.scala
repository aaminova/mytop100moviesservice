package movies.configuration

import com.typesafe.config.ConfigFactory
import zio.TaskLayer
import zio.ZIO
import zio.ZLayer
import zio.config.magnolia.descriptor
import zio.config.typesafe.TypesafeConfig

final case class AppConfig(
    api: ApiConfig,
    db: DbConfig
)

object AppConfig {
  val layer: TaskLayer[AppConfig] = ZLayer.scoped {
    ZIO
      .attempt(ConfigFactory.load().getConfig("movies"))
      .absorb
      .flatMap(cfg =>
        TypesafeConfig
          .fromTypesafeConfig(
            ZIO.succeed(cfg),
            descriptor[AppConfig]
          )
          .build
      )
      .map(_.get)
  }
}
