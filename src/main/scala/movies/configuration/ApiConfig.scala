package movies.configuration

case class ApiConfig(endpoint: String, port: Int)
