package movies.db

import io.getquill.SnakeCase
import io.getquill.doobie.DoobieContext
import movies.model._

trait Schema {
  val dc: DoobieContext.Postgres[SnakeCase]

  import dc._

  val movies = quote {
    querySchema[Movie]("movie")
  }

  val users = quote {
    querySchema[User]("users")
  }

  val roles = quote {
    querySchema[Role]("role")
  }
}
