package movies.db.role

import io.getquill.SnakeCase
import io.getquill.doobie.DoobieContext
import movies.db.Schema
import movies.db.TransactorManager
import movies.model.Role
import zio.Task

trait RoleDao {
  def getByName(name: Role.Text): Task[Option[Role]]
  def getById(id: Role.Id): Task[Option[Role]]
}

final class RoleDaoImpl(tm: TransactorManager) extends RoleDao with Schema {
  import doobie.implicits._
  import zio.interop.catz.asyncInstance

  private[this] val xa = tm.getTransactor

  override val dc = new DoobieContext.Postgres(SnakeCase)
  import dc._

  override def getByName(name: Role.Text): Task[Option[Role]] =
    run(quote(unquote(roles).filter(_.name == lift(name)))).transact(xa).map(_.headOption)

  override def getById(id: Role.Id): Task[Option[Role]] =
    run(quote(unquote(roles).filter(_.id == lift(id)))).transact(xa).map(_.headOption)
}
