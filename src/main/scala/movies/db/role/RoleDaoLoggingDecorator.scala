package movies.db.role

import movies.db.TransactorManager
import movies.model.Role
import tofu.syntax.loggable.TofuLoggableOps
import zio._

final class RoleDaoLoggingDecorator(roleDao: RoleDao) extends RoleDao {
  override def getByName(name: Role.Text): Task[Option[Role]] =
    ZIO.logInfo("Поиск роли в БД") @@ ZIOAspect.annotated("role" -> name.logShow) *>
      roleDao.getByName(name)

  override def getById(id: Role.Id): Task[Option[Role]] =
    ZIO.logInfo("Поиск роли в БД") @@ ZIOAspect.annotated("roleId" -> id.logShow) *>
      roleDao.getById(id)
}

object RoleDaoLoggingDecorator {
  val live: RLayer[TransactorManager, RoleDao] = ZLayer(for {
    tm <- ZIO.service[TransactorManager]
    roleDao = new RoleDaoImpl(tm)
  } yield new RoleDaoLoggingDecorator(roleDao))
}
