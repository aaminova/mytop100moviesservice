package movies.db.movie

import io.getquill.SnakeCase
import io.getquill.doobie.DoobieContext
import movies.db.Schema
import movies.db.TransactorManager
import movies.model._
import zio.Task

trait MovieDao {
  def add(movie: Movie): Task[Unit]
  def get(id: Movie.Id): Task[Option[Movie]]
  def getAll: Task[List[Movie]]
  def editDescription(id: Movie.Id, description: String): Task[Unit]
  def editRating(id: Movie.Id, rating: Double): Task[Unit]
  def delete(id: Movie.Id): Task[Unit]
}

final class MovieDaoImpl(tm: TransactorManager) extends MovieDao with Schema {
  import doobie.implicits._
  import zio.interop.catz.asyncInstance

  private[this] val xa = tm.getTransactor

  val dc = new DoobieContext.Postgres[SnakeCase](SnakeCase)
  import dc._

  override def add(movie: Movie): Task[Unit] =
    run(quote(unquote(movies).insertValue(lift(movie)))).transact(xa).as(())

  override def get(id: Movie.Id): Task[Option[Movie]] =
    run(quote(unquote(movies).filter(_.id == lift(id)))).transact(xa).map(_.headOption)

  override def getAll: Task[List[Movie]] =
    run(quote(unquote(movies))).transact(xa)

  override def editDescription(id: Movie.Id, description: String): Task[Unit] =
    run(
      quote(
        unquote(movies)
          .filter(_.id == lift(id))
          .update(_.description -> lift(Some(description): Option[String]))
      )
    ).transact(xa).as(())

  override def editRating(id: Movie.Id, rating: Double): Task[Unit] =
    run(
      quote(
        unquote(movies)
          .filter(_.id == lift(id))
          .update(_.rating -> lift(rating))
      )
    ).transact(xa).as(())

  override def delete(id: Movie.Id): Task[Unit] =
    run(quote(unquote(movies).filter(_.id == lift(id)).delete)).transact(xa).as(())
}
