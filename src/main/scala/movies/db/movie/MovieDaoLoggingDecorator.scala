package movies.db.movie

import movies.db.TransactorManager
import movies.model.Movie
import zio._

final class MovieDaoLoggingDecorator(movieDao: MovieDao) extends MovieDao {
  override def add(movie: Movie): Task[Unit] =
    ZIO.logInfo("Добавление фильма в БД") @@ ZIOAspect.annotated("movieId" -> movie.id.toString) *>
      movieDao.add(movie)

  override def get(id: Movie.Id): Task[Option[Movie]] =
    ZIO.logInfo("Поиск фильма в БД") @@ ZIOAspect.annotated("movieId" -> id.toString) *>
      movieDao.get(id)

  override def getAll: Task[List[Movie]] =
    ZIO.logInfo("Поиск фильмов в БД") *>
      movieDao.getAll

  override def editDescription(id: Movie.Id, description: String): Task[Unit] =
    ZIO.logInfo("Изменение описания фильма в БД") @@ ZIOAspect.annotated("movieId" -> id.toString) *>
      movieDao.editDescription(id, description)

  override def editRating(id: Movie.Id, rating: Double): Task[Unit] =
    ZIO.logInfo("Изменение рейтинга фильма в БД") @@ ZIOAspect.annotated("movieId" -> id.toString) *>
      movieDao.editRating(id, rating)

  override def delete(id: Movie.Id): Task[Unit] =
    ZIO.logInfo("Удаление фильма из БД") @@ ZIOAspect.annotated("movieId" -> id.toString) *>
      movieDao.delete(id)
}

object MovieDaoLoggingDecorator {
  val live: RLayer[TransactorManager, MovieDao] = ZLayer(for {
    tm <- ZIO.service[TransactorManager]
    movieDao = new MovieDaoImpl(tm)
  } yield new MovieDaoLoggingDecorator(movieDao))
}
