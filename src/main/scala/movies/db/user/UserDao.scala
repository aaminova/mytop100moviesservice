package movies.db.user

import io.getquill.SnakeCase
import io.getquill.doobie.DoobieContext
import movies.db.Schema
import movies.db.TransactorManager
import movies.model.Role
import movies.model.User
import zio.Task

trait UserDao {
  def get(login: String): Task[Option[User]]
  def add(user: User): Task[Unit]
  def delete(login: String): Task[Unit]
  def changeRole(login: String, roleId: Role.Id): Task[Unit]
}

final class UserDaoImpl(tm: TransactorManager) extends UserDao with Schema {
  import doobie.implicits._
  import zio.interop.catz.asyncInstance

  private[this] val xa = tm.getTransactor

  override val dc = new DoobieContext.Postgres(SnakeCase)
  import dc._

  override def get(login: String): Task[Option[User]] =
    run(quote(unquote(users).filter(_.login == lift(login)))).transact(xa).map(_.headOption)

  override def add(user: User): Task[Unit] =
    run(quote(unquote(users).insertValue(lift(user)))).transact(xa).as(())

  override def delete(login: String): Task[Unit] =
    run(quote(unquote(users).filter(_.login == lift(login)).delete)).transact(xa).as(())

  override def changeRole(login: String, roleId: Role.Id): Task[Unit] =
    run(
      quote(
        unquote(users)
          .filter(_.login == lift(login))
          .update(_.roleId -> lift(roleId))
      )
    ).transact(xa).as(())
}
