package movies.db.user

import movies.db.TransactorManager
import movies.model.Role
import movies.model.User
import zio._

final class UserDaoLoggingDecorator(userDao: UserDao) extends UserDao {
  override def add(user: User): Task[Unit] =
    ZIO.logInfo("Добавление пользователя в БД") @@ ZIOAspect.annotated(
      "userId" -> user.id.toString,
      "login"  -> user.login
    ) *>
      userDao.add(user)

  override def get(login: String): Task[Option[User]] =
    ZIO.logInfo("Поиск пользователя в БД") @@ ZIOAspect.annotated("login" -> login) *>
      userDao.get(login)

  override def changeRole(login: String, roleId: Role.Id): Task[Unit] =
    ZIO.logInfo("Изменение роли пользователя в БД") @@ ZIOAspect.annotated("login" -> login) *>
      userDao.changeRole(login, roleId)

  override def delete(login: String): Task[Unit] =
    ZIO.logInfo("Удаление пользователя из БД") @@ ZIOAspect.annotated("login" -> login) *>
      userDao.delete(login)
}

object UserDaoLoggingDecorator {
  val live: RLayer[TransactorManager, UserDao] = ZLayer(for {
    tm <- ZIO.service[TransactorManager]
    movieDao = new UserDaoImpl(tm)
  } yield new UserDaoLoggingDecorator(movieDao))
}
