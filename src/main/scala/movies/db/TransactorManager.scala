package movies.db

import doobie.Transactor
import doobie.util.transactor.Transactor.Aux
import movies.configuration.DbConfig
import zio.RLayer
import zio.Task
import zio.ZLayer
import zio.interop.catz.asyncInstance

trait TransactorManager {
  def getTransactor: Aux[Task, Unit]
}

final class TransactorManagerImpl(config: DbConfig) extends TransactorManager {
  override def getTransactor: Aux[Task, Unit] =
    Transactor.fromDriverManager[Task](config.driver, config.url, config.user, config.password)
}

object TransactorManager {
  val live: RLayer[DbConfig, TransactorManager] = ZLayer.fromFunction(new TransactorManagerImpl(_))
}
