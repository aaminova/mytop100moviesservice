package movies.routing

import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.duration._

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import fr.davit.akka.http.metrics.core.HttpMetrics.enrichHttp
import fr.davit.akka.http.metrics.prometheus.PrometheusRegistry
import movies.configuration.ApiConfig
import movies.metrics.Metrics
import movies.routing.routes.MovieRoute
import movies.routing.routes.UserRoute
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter
import sttp.tapir.server.akkahttp.AkkaHttpServerOptions
import sttp.tapir.swagger.SwaggerUIOptions
import sttp.tapir.swagger.bundle.SwaggerInterpreter
import zio.Cause
import zio.ZIO
import zio.ZLayer

object Server {
  import akka.http.scaladsl.server.RouteConcatenation._

  val live = ZLayer.scoped {
    for {
      runtime     <- ZIO.runtime[Any]
      actorSystem <- ZIO.service[ActorSystem]
      apiConfig   <- ZIO.service[ApiConfig]
      movieRoute  <- ZIO.service[MovieRoute]
      userRoute   <- ZIO.service[UserRoute]
      metrics     <- ZIO.service[Metrics]

      routes = movieRoute.routes ++ userRoute.routes :+ metrics.tapirMetrics.metricsEndpoint

      swagger = SwaggerInterpreter(
        swaggerUIOptions = SwaggerUIOptions.default.pathPrefix(List("docs", "myTop100Movies"))
      ).fromServerEndpoints[Future](
        endpoints = routes,
        title = "myTop100Movies",
        version = "0.0.3"
      )

      binding <- ZIO.acquireRelease {
        ZIO.fromFuture { _ =>
          implicit val system: ActorSystem  = actorSystem
          implicit val ec: ExecutionContext = actorSystem.getDispatcher

          val serverLog = new ZIOServerLog(runtime)
          val options = AkkaHttpServerOptions.customiseInterceptors
            .serverLog(serverLog)
            .metricsInterceptor(metrics.tapirMetrics.metricsInterceptor())

          val tapirRoutes   = AkkaHttpServerInterpreter(options.options).toRoute(routes)
          val swaggerRoutes = AkkaHttpServerInterpreter().toRoute(swagger)

          val akkaHttpRegistry = PrometheusRegistry(metrics.collectorRegistry)

          Http()
            .newMeteredServerAt(apiConfig.endpoint, apiConfig.port, akkaHttpRegistry)
            .bind(tapirRoutes ~ swaggerRoutes)
        }
      } { binding =>
        ZIO.fromFuture(_ => binding.terminate(15.seconds)).catchAll { e =>
          ZIO.logErrorCause("Failed to terminate http server", Cause.fail(e))
        } <* ZIO.logInfo("Http server terminated successfully")
      }
      _ <- ZIO.logInfo(
        s"Server started at: ${binding.localAddress}/; " +
          s"Swagger started at: ${binding.localAddress}/docs/myTop100Movies"
      )
    } yield binding
  }
}
