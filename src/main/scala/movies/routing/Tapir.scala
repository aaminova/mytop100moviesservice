package movies.routing

import scala.concurrent.Future

import zio.Runtime
import zio.Task
import zio.Unsafe

object Tapir {
  def wrapLogic[A](action: Task[A])(implicit runtime: Runtime[Any]): Future[A] =
    Unsafe.unsafe { implicit unsafe =>
      runtime.unsafe.runToFuture(action)
    }
}
