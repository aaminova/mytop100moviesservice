package movies.routing

import scala.concurrent.Future

import sttp.tapir.DecodeResult
import sttp.tapir.model.ServerRequest
import sttp.tapir.server.interceptor.DecodeFailureContext
import sttp.tapir.server.interceptor.DecodeSuccessContext
import sttp.tapir.server.interceptor.SecurityFailureContext
import sttp.tapir.server.interceptor.log.ExceptionContext
import sttp.tapir.server.interceptor.log.ServerLog
import sttp.tapir.server.model.ServerResponse
import zio.Cause
import zio.Runtime
import zio.UIO
import zio.Unsafe
import zio.ZIO
import zio.ZIOAspect

final class ZIOServerLog(runtime: Runtime[Any]) extends ServerLog[Future] {
  override type TOKEN = Long

  override def requestReceived(request: ServerRequest, token: TOKEN): Future[Unit] =
    unsafeRun {
      ZIO.logInfo("http request") @@ ZIOAspect.annotated(
        "uri"    -> request.uri.toString(),
        "method" -> request.method.toString,
      )
    }

  override def requestToken: Long = 0

  override def decodeFailureNotHandled(ctx: DecodeFailureContext, token: Long): Future[Unit] =
    unsafeRun {
      ZIO.logErrorCause(
        "Decode failure",
        decodeException(ctx).map(Cause.fail(_)).getOrElse(Cause.empty)
      ) @@ ZIOAspect.annotated(
        "uri"     -> ctx.request.uri.toString(),
        "method"  -> ctx.request.method.toString,
        "payload" -> ctx.failingInput.show,
        "failure" -> ctx.failure.toString,
      )
    }

  override def decodeFailureHandled(ctx: DecodeFailureContext, response: ServerResponse[_], token: Long): Future[Unit] =
    unsafeRun {
      ZIO.logErrorCause(
        "Decode failure with response",
        decodeException(ctx).map(Cause.fail(_)).getOrElse(Cause.empty)
      ) @@ ZIOAspect.annotated(
        "uri"        -> ctx.request.uri.toString(),
        "method"     -> ctx.request.method.toString,
        "payload"    -> ctx.failingInput.show,
        "statusCode" -> response.code.toString,
        "response"   -> response.body.toString
      )
    }

  override def securityFailureHandled(
      ctx: SecurityFailureContext[Future, _],
      response: ServerResponse[_],
      token: Long
  ): Future[Unit] =
    unsafeRun {
      ZIO.logError("handled request") @@ ZIOAspect.annotated(
        "phase"      -> "response",
        "uri"        -> ctx.request.uri.toString(),
        "method"     -> ctx.request.method.toString,
        "statusCode" -> response.code.toString,
      )
    }

  override def requestHandled(
      ctx: DecodeSuccessContext[Future, _, _, _],
      response: ServerResponse[_],
      token: Long
  ): Future[Unit] =
    unsafeRun {
      ZIO.logInfo("handled request") @@ ZIOAspect.annotated(
        "uri"        -> ctx.request.uri.toString(),
        "method"     -> ctx.request.method.toString,
        "statusCode" -> response.code.toString,
        "payload"    -> response.body.toString
      )
    }

  override def exception(ctx: ExceptionContext[_, _], ex: Throwable, token: Long): Future[Unit] =
    unsafeRun {
      ZIO.logErrorCause("Error handling request", Cause.fail(ex)) @@ ZIOAspect.annotated(
        "uri"    -> ctx.request.uri.toString(),
        "method" -> ctx.request.method.toString,
      )
    }

  private def decodeException(ctx: DecodeFailureContext): Option[Throwable] =
    ctx.failure match {
      case DecodeResult.Error(_, error) => Some(error)
      case _                            => None
    }

  private def unsafeRun[A](log: UIO[A]): Future[A] =
    Unsafe.unsafe(implicit unsafe => runtime.unsafe.runToFuture(log))

}
