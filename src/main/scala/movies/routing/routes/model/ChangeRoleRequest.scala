package movies.routing.routes.model

import derevo.circe.decoder
import derevo.circe.encoder
import derevo.derive
import movies.model.Role
import sttp.tapir.derevo.schema
import tofu.logging.derivation.loggable

@derive(encoder, decoder, schema, loggable)
final case class ChangeRoleRequest(role: Role.Type)
