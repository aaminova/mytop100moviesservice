package movies.routing.routes.model

import derevo.circe.decoder
import derevo.circe.encoder
import derevo.derive
import sttp.tapir.derevo.schema
import tofu.logging.derivation.hidden
import tofu.logging.derivation.loggable

@derive(encoder, decoder, schema, loggable)
final case class RegisterRequest(login: String, @hidden password: String)
