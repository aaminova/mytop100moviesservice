package movies.routing.routes

import scala.concurrent.Future

import movies.logic.AuthenticationService
import movies.logic.MyTop100MoviesService
import movies.logic.MyTop100MoviesServiceError.errorMapper
import movies.model._
import movies.routing.Tapir.wrapLogic
import sttp.tapir._
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.model.UsernamePassword
import zio._

final class MovieRoute(myTop100MoviesService: MyTop100MoviesService, authService: AuthenticationService)(implicit
    runtime: Runtime[Any]
) {
  private val baseRoute =
    endpoint
      .in("api" / "myTop100Movies" / "v1" / "movies")
      .tag("Movies")
      .errorOut(errorMapper)
      .securityIn(auth.basic[UsernamePassword]())
      .serverSecurityLogic[User, Future](up => wrapLogic(authService.securityLogic(up)))

  private val addMovieRoute = baseRoute.post
    .summary("Добавление фильма в коллекцию")
    .in("add")
    .in(jsonBody[Movie])
    .out(jsonBody[String])
    .serverLogic(user => body => wrapLogic(myTop100MoviesService.addMovie(user, body)))

  private val getMovieRoute = baseRoute.get
    .summary("Фильм по id")
    .in(path[Movie.Id]("movieId"))
    .out(jsonBody[Movie])
    .serverLogic(user => id => wrapLogic(myTop100MoviesService.getMovie(user, id)))

  private val getMoviesRoute = baseRoute.get
    .summary("Все фильмы из списка")
    .in("all")
    .out(jsonBody[List[Movie]])
    .serverLogic(user => _ => wrapLogic(myTop100MoviesService.getMovies(user)))

  private val editMovieDescriptionRoute = baseRoute.post
    .summary("Изменение описания фильма по id")
    .in(path[Movie.Id]("id"))
    .in("description")
    .in(jsonBody[String])
    .out(jsonBody[String])
    .serverLogic { user =>
      { case (id, description) =>
        wrapLogic(myTop100MoviesService.editMovieDescription(user, id, description))
      }
    }

  private val editMovieRatingRoute = baseRoute.post
    .summary("Изменение рейтинга фильма по id")
    .in(path[Movie.Id]("id"))
    .in("rating")
    .in(jsonBody[Double])
    .out(jsonBody[String])
    .serverLogic { user =>
      { case (id, rating) =>
        wrapLogic(myTop100MoviesService.editMovieRating(user, id, rating))
      }
    }

  private val deleteMovieRoute = baseRoute.delete
    .summary("Удаление фильма по id")
    .in(path[Movie.Id]("id"))
    .out(jsonBody[String])
    .serverLogic(user => id => wrapLogic(myTop100MoviesService.deleteMovie(user, id)))

  /*
   * POST   /myTop100Movies/v1/movies/add
   * GET    /myTop100Movies/v1/movies/all
   * GET    /myTop100Movies/v1/movies/{movieId}
   * POST   /myTop100Movies/v1/movies/{movieId}/rating
   * POST   /myTop100Movies/v1/movies/{movieId}/description
   * DELETE /myTop100Movies/v1/movies/{movieId}
   *  */

  val routes = List(
    addMovieRoute,
    getMoviesRoute,
    getMovieRoute,
    editMovieDescriptionRoute,
    editMovieRatingRoute,
    deleteMovieRoute
  )
}

object MovieRoute {
  val live: RLayer[MyTop100MoviesService & AuthenticationService, MovieRoute] = ZLayer(
    for {
      runtime      <- ZIO.runtime[Any]
      movieService <- ZIO.service[MyTop100MoviesService]
      authService  <- ZIO.service[AuthenticationService]
    } yield new MovieRoute(movieService, authService)(runtime)
  )
}
