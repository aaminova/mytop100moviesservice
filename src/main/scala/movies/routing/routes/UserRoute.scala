package movies.routing.routes

import scala.concurrent.Future

import movies.logic.AuthenticationService
import movies.logic.MyTop100MoviesServiceError.errorMapper
import movies.logic.UserService
import movies.model.User
import movies.routing.Tapir.wrapLogic
import movies.routing.routes.model.ChangeRoleRequest
import movies.routing.routes.model.RegisterRequest
import sttp.tapir._
import sttp.tapir.json.circe.jsonBody
import sttp.tapir.model.UsernamePassword
import zio._

final class UserRoute(authService: AuthenticationService, userService: UserService)(implicit runtime: Runtime[Any]) {
  private val baseRoute =
    endpoint
      .in("api" / "myTop100Movies" / "v1" / "users")
      .tag("Users")
      .errorOut(errorMapper)
      .securityIn(auth.basic[UsernamePassword]())
      .serverSecurityLogic[User, Future](up => wrapLogic(authService.securityLogic(up)))

  private val registerRoute = baseRoute.post
    .summary("Регистрация нового пользователя")
    .in("register")
    .in(jsonBody[RegisterRequest])
    .out(jsonBody[String])
    .serverLogic(user => body => wrapLogic(userService.register(user, body)))

  private val getRoute = baseRoute.get
    .summary("Информация о пользователе")
    .in(path[String]("login"))
    .out(jsonBody[User])
    .serverLogic(user => login => wrapLogic(userService.getUser(user, login)))

  private val changeRoleRoute = baseRoute.post
    .summary("Изменение прав пользователя")
    .in(path[String]("login"))
    .in("role")
    .in(jsonBody[ChangeRoleRequest])
    .out(jsonBody[String])
    .serverLogic(user => { case (login, body) =>
      wrapLogic(userService.changeRole(user, login, body))
    })

  private val deleteRoute = baseRoute.delete
    .summary("Удаление пользователя")
    .in(path[String]("login"))
    .out(jsonBody[String])
    .serverLogic(user => login => wrapLogic(userService.deleteUser(user, login)))

  val routes = List(registerRoute, getRoute, changeRoleRoute, deleteRoute)

}

object UserRoute {
  val live: RLayer[UserService & AuthenticationService, UserRoute] = ZLayer(
    for {
      runtime     <- ZIO.runtime[Any]
      userService <- ZIO.service[UserService]
      authService <- ZIO.service[AuthenticationService]
    } yield new UserRoute(authService, userService)(runtime)
  )
}
