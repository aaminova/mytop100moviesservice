FROM sbtscala/scala-sbt:eclipse-temurin-11.0.14.1_1.6.2_2.13.8 as build0
WORKDIR /opt/docker
COPY . .
RUN sbt compile
RUN sbt "Docker / stage"

FROM openjdk:8 as rights
WORKDIR /opt/moviesApp
COPY --from=build0 /opt/docker/target/docker/stage/2/opt /2/opt
COPY --from=build0 /opt/docker/target/docker/stage/4/opt /4/opt
USER root
RUN ["chmod", "-R", "u=rX,g=rX", "/2/opt/docker"]
RUN ["chmod", "-R", "u=rX,g=rX", "/4/opt/docker"]
RUN ["chmod", "u+x,g+x", "/4/opt/docker/bin/moviesapp"]

FROM eclipse-temurin:11
USER root
RUN id -u demiourgos728 1>/dev/null 2>&1 || (( getent group 0 1>/dev/null 2>&1 || ( type groupadd 1>/dev/null 2>&1 && groupadd -g 0 root || addgroup -g 0 -S root )) && ( type useradd 1>/dev/null 2>&1 && useradd --system --create-home --uid 1001 --gid 0 demiourgos728 || adduser -S -u 1001 -G root demiourgos728 ))
WORKDIR /opt/docker
COPY --from=rights --chown=demiourgos728:root /2/opt/docker /opt/docker
COPY --from=rights --chown=demiourgos728:root /4/opt/docker /opt/docker
EXPOSE 8080
USER 1001:0
ENTRYPOINT ["bash", "/opt/docker/bin/moviesapp"]
