create table movie
(
    id          UUID PRIMARY KEY,
    title       VARCHAR NOT NULL,
    description VARCHAR,
    rating      REAL NOT NULL DEFAULT 0
);
